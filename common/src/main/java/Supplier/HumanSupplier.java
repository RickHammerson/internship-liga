package Supplier;

import java.util.Random;

public class HumanSupplier {

    public static Human returnNewHuman() {
        return new Human(returnRandomString(),returnRandomString(),returnRandomString(),returnRandomAge(),returnRandomString(),returnRandomString());
    }

    public static Human returnNewHuman(String name, String subName, String lastName, int age, String sex, String birthDate) {
        return new Human(name,subName,lastName,age,sex,birthDate);
    }

    private static int returnRandomAge(){
        return new Random().nextInt(100) + 2;
    }

    private static String returnRandomString(){
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder temp=null;
        int countOfChar=new Random().nextInt(7) + 2;
        for (int i = 0; i < countOfChar; i++) {
            temp.append(alphabet.charAt(new Random().nextInt(alphabet.length())));
        }
        return temp.toString();
    }
}
