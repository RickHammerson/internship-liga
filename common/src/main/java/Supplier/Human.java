package Supplier;

import java.util.Objects;

public class Human {
    private String name;
    private String subName;
    private String LastName;
    private int age;
    private String sex;
    private String birthDate;

    public Human() {
    }

    public Human(String name, String subName, String lastName) {
        this.name = name;
        this.subName = subName;
        LastName = lastName;
    }

    public Human(String name, String subName, String lastName, int age, String sex, String birthDate) {
        this.name = name;
        this.subName = subName;
        LastName = lastName;
        this.age = age;
        this.sex = sex;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public String getSubName() {
        return subName;
    }

    public String getLastName() {
        return LastName;
    }

    public int getAge() {
        return age;
    }

    public String getSex() {
        return sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                name.equals(human.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", subName='" + subName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", birthDate='" + birthDate + '\'' +
                '}';
    }
}
