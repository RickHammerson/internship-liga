import Supplier.Human;
import Supplier.HumanSupplier;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Application {
    public static void main(String[] args) {
        HumanSupplier humanSupplier = new HumanSupplier();
        List<Human> listOfHuman = new ArrayList<Human>();
        listOfHuman.add(humanSupplier.returnNewHuman("first","first","first",15,"male","15-07-2020"));
        listOfHuman.add(humanSupplier.returnNewHuman("second","second","second",25,"female","15-07-2020"));
        listOfHuman.add(humanSupplier.returnNewHuman("third","third","third",15,"male","15-07-2020"));
        listOfHuman.add(humanSupplier.returnNewHuman("fourth","fourth","fourth",11,"cup","15-07-2020"));
        listOfHuman.forEach(a->System.out.println(a));

        System.out.println();
        listOfHuman=listOfHuman.stream().filter(human -> (human.getAge() >= 12) & (human.getSex().equals("male"))).collect(Collectors.toList());
        listOfHuman.forEach(a->System.out.println(a));
    }
}
